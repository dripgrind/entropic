{
    "id": "e7790200-3d7f-4488-a21d-2aa93b787ac4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c9665166-da61-4caa-a131-cbc0d62fe8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7790200-3d7f-4488-a21d-2aa93b787ac4",
            "compositeImage": {
                "id": "c7907295-439e-4cf0-adb4-94bfe18e9906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9665166-da61-4caa-a131-cbc0d62fe8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c631e61e-f749-4a03-9e93-94b635b15fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9665166-da61-4caa-a131-cbc0d62fe8e4",
                    "LayerId": "99e32dca-9f2c-4e5f-aa4b-bfaa6c699a94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99e32dca-9f2c-4e5f-aa4b-bfaa6c699a94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7790200-3d7f-4488-a21d-2aa93b787ac4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}