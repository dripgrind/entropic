{
    "id": "7173160f-7fc7-4680-9e18-fab9b8f4ca33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rec",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8e575a3f-43fa-43d1-bc10-67ca8ff3ef21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7173160f-7fc7-4680-9e18-fab9b8f4ca33",
            "compositeImage": {
                "id": "16135d4a-515e-4ccd-a9f8-3e0d41432824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e575a3f-43fa-43d1-bc10-67ca8ff3ef21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74dd967e-fbd3-49e2-8d77-79bab8dfd7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e575a3f-43fa-43d1-bc10-67ca8ff3ef21",
                    "LayerId": "816f38a7-aec5-432b-bfa2-aa829c115d43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "816f38a7-aec5-432b-bfa2-aa829c115d43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7173160f-7fc7-4680-9e18-fab9b8f4ca33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 172,
    "xorig": 0,
    "yorig": 0
}