{
    "id": "4c9238f0-da1d-410e-b731-20cae8a01ac9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rewind",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ffa03863-b02b-4d1d-9b4b-9d172c9b6ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c9238f0-da1d-410e-b731-20cae8a01ac9",
            "compositeImage": {
                "id": "aeb56656-9692-4c48-9c75-14d7b78ee874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa03863-b02b-4d1d-9b4b-9d172c9b6ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cd48db8-b364-47c6-a3c6-24239f60ec5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa03863-b02b-4d1d-9b4b-9d172c9b6ecf",
                    "LayerId": "9834a3fb-16d0-45c1-920b-e9a44bb83ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "9834a3fb-16d0-45c1-920b-e9a44bb83ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c9238f0-da1d-410e-b731-20cae8a01ac9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}