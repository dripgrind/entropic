{
    "id": "3b7b6436-5edc-44cb-9bff-1a82cf6c255e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 50,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "725d565b-f8bb-42ad-bbdc-11bd25bb53ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b7b6436-5edc-44cb-9bff-1a82cf6c255e",
            "compositeImage": {
                "id": "1ff09cda-750c-4781-b8e6-2a550a923519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "725d565b-f8bb-42ad-bbdc-11bd25bb53ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95b3893-a29c-43d3-aab8-5c7ce5524e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "725d565b-f8bb-42ad-bbdc-11bd25bb53ed",
                    "LayerId": "55c8b56b-b2b2-4e66-a34a-acca5b92d838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "55c8b56b-b2b2-4e66-a34a-acca5b92d838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b7b6436-5edc-44cb-9bff-1a82cf6c255e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 26,
    "yorig": 32
}