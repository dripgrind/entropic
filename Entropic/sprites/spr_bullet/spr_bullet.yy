{
    "id": "f3efff10-13b9-4016-a88c-291216c032b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "78814369-788a-4435-8485-4a370035405b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3efff10-13b9-4016-a88c-291216c032b8",
            "compositeImage": {
                "id": "58dee891-112a-4892-9e96-56203cd4b842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78814369-788a-4435-8485-4a370035405b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad202bb1-0c0c-42f4-9157-22c8ff98b302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78814369-788a-4435-8485-4a370035405b",
                    "LayerId": "c4840652-d0e6-4e7d-a11f-b38367380c3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "c4840652-d0e6-4e7d-a11f-b38367380c3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3efff10-13b9-4016-a88c-291216c032b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}