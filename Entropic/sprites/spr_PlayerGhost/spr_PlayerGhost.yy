{
    "id": "3c2677a0-da2c-4981-af9c-ab622f862eb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerGhost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a170bbb8-1263-4cd1-bb81-efeae4074e21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c2677a0-da2c-4981-af9c-ab622f862eb1",
            "compositeImage": {
                "id": "3469e930-d6ef-417f-b671-a506d359cdbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a170bbb8-1263-4cd1-bb81-efeae4074e21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce0312f-1de6-4bf0-a631-f407b02f1601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a170bbb8-1263-4cd1-bb81-efeae4074e21",
                    "LayerId": "c74ce791-eee5-4258-9e7e-69ac348ab2a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c74ce791-eee5-4258-9e7e-69ac348ab2a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c2677a0-da2c-4981-af9c-ab622f862eb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}