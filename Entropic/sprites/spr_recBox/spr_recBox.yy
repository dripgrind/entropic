{
    "id": "915ae14d-7142-4131-86d9-b67e1cb97fae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "16a8126f-bdc8-456e-b0e6-536546578330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "915ae14d-7142-4131-86d9-b67e1cb97fae",
            "compositeImage": {
                "id": "55776a76-c102-4bdf-8eeb-354f1dd7d444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a8126f-bdc8-456e-b0e6-536546578330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5542012-205d-43dd-93b0-26272268b5ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a8126f-bdc8-456e-b0e6-536546578330",
                    "LayerId": "cff46abc-1f0d-4d0a-be17-44ce040e0c5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "cff46abc-1f0d-4d0a-be17-44ce040e0c5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "915ae14d-7142-4131-86d9-b67e1cb97fae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}