{
    "id": "f13ed15a-e445-4ee2-8aca-713db6a1bdfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3c346590-567e-478a-b568-d79dd124fc3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f13ed15a-e445-4ee2-8aca-713db6a1bdfb",
            "compositeImage": {
                "id": "80f403dc-e5f9-4595-9855-7c8c69339118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c346590-567e-478a-b568-d79dd124fc3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5840d197-d938-4e09-91b0-64698c66c23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c346590-567e-478a-b568-d79dd124fc3e",
                    "LayerId": "aab009f5-9486-4fba-ab61-166624844f5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aab009f5-9486-4fba-ab61-166624844f5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f13ed15a-e445-4ee2-8aca-713db6a1bdfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}