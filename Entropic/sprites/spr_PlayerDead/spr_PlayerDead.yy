{
    "id": "314aaf41-d506-4fed-9ac6-f9ee8f932cfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1c19aff4-29ea-4411-9bad-81276a262977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "314aaf41-d506-4fed-9ac6-f9ee8f932cfa",
            "compositeImage": {
                "id": "8b52a8a6-afa3-4cab-b042-6e5df23c9f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c19aff4-29ea-4411-9bad-81276a262977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "262d094b-1248-44fe-ae2c-7813f1ce5212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c19aff4-29ea-4411-9bad-81276a262977",
                    "LayerId": "8388e702-d879-4e63-84a2-0213535699d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8388e702-d879-4e63-84a2-0213535699d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "314aaf41-d506-4fed-9ac6-f9ee8f932cfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}