{
    "id": "b13ee324-ea25-4629-b6a5-6e1d9821cdd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key_return",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "847d2942-3dd1-4251-8309-a5ae75dac4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b13ee324-ea25-4629-b6a5-6e1d9821cdd0",
            "compositeImage": {
                "id": "0608a25f-5836-47a1-a962-19f8d3ce4d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847d2942-3dd1-4251-8309-a5ae75dac4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cae825e-b1a0-4fe2-b4fe-5fb037fabba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847d2942-3dd1-4251-8309-a5ae75dac4ad",
                    "LayerId": "4e9ed744-c283-4dc7-b940-32254a79a43f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "4e9ed744-c283-4dc7-b940-32254a79a43f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b13ee324-ea25-4629-b6a5-6e1d9821cdd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 101,
    "xorig": 0,
    "yorig": 0
}