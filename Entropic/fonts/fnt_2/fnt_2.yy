{
    "id": "06a43641-6686-4a25-afed-8d51c6e1f6cf",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "adee2be6-dccf-4fc4-8087-bccd2e4390fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4c85ce58-d93b-49ae-a9b8-6aba2db63318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 21,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b7e36889-8c6e-47b5-8649-4bb9a1e52d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 42,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dc7a1a1d-db9d-4446-8ccc-6828ce57c6a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 63,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ae4cb424-390f-45f3-a721-61a6f981723f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 84,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "36d9558f-5ee8-4bc0-95b4-0af2905ad2ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 105,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5df56dde-4b85-4377-be99-bcd47f3a9b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a1744542-3cf0-4c4c-a09a-c01728e3f51a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 147,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6a39a65b-755e-4a03-adf2-386ed5338a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 168,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "52c2da7e-e631-4198-b2e0-86026aeadef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 189,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "210e56f5-0a11-455c-b9ed-5a57c636a6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 0,
                "y": 25
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4e479850-178a-4d97-add1-70e08cb405a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 21,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d0af5c1a-7f36-415f-94a4-2fd159c412b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 42,
                "y": 25
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "607d80b0-1964-40fa-97e9-c04f31748c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 25
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "44d68012-22f0-4c55-b83f-183fd26bf70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 84,
                "y": 25
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f87f12b1-ecd5-4282-b441-7386549dfed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 105,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "281a758e-d507-4cb7-8b15-0c2e307b16f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 126,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "79fa6507-1101-4281-8e3d-c949d49a70b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 147,
                "y": 25
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1b851c59-a261-4952-abee-a67a98e2dfec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 168,
                "y": 25
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8547685d-5b09-4a05-8f1e-7d24c17da13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "01c4af16-f528-4c3b-9a82-03d28fc808cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 0,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a9888a25-2565-40a9-b31a-7504ac01df31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 21,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "36ddd434-4ff2-491a-80eb-0fd636cd8a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 42,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6787dfe6-5af6-419e-a7af-5b59fcdd8d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 63,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9dd0450b-6984-46e0-8ee0-8e03d3949553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e400a0e5-adba-41d5-aeed-c4c8d8afadb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ee0067b6-9bea-4e6e-a09c-5bc7d9cc5d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 126,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "27d16285-6559-4a63-806b-6e09dbb97334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 147,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "eebab5c5-a347-4f88-89f0-c5f73810f568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b594cb72-fbde-436d-9746-3db3bc2b0ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eeceb946-7e14-48ca-84db-37e48112fdbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 0,
                "y": 75
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "04830b8f-ce53-4707-8e8b-350c7a8531d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 21,
                "y": 75
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7fc778ab-ea7a-4ca8-b743-56956e47f593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 42,
                "y": 75
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2677e0b2-f7f3-4e4e-9f70-be89f68a8653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 63,
                "y": 75
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "531a32fb-5a5c-40c6-912d-4ad3c0c77ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 84,
                "y": 75
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cc4215a8-a6c6-4f52-b394-1202408c6bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 105,
                "y": 75
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "df229293-476b-4cf0-b068-f25b9cf902b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 126,
                "y": 75
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "25656e37-5499-46cf-8723-d915def634d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 147,
                "y": 75
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a57180df-48d2-4976-9e32-29b65e19e307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 168,
                "y": 75
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7b6f8dd6-8d59-4754-b86e-9e4817262f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 189,
                "y": 75
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1147fdd5-19e9-48cc-b2eb-ae528f638dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 0,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3386d903-baed-4eed-aaf6-4e12628a6a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 21,
                "y": 100
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ddbf719a-2f0e-47b3-97af-625d4f6cbb04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "687d8cf2-5f2a-4e69-8013-468489e448ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 63,
                "y": 100
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "426ef98f-342a-4f3c-97b3-973a56c6a7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 84,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d69b68dc-5495-409b-9588-19e9fb0d7a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 105,
                "y": 100
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7422e87a-055d-43e8-bee4-443717a6c092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 126,
                "y": 100
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "13e711de-e583-4fe4-b194-4cedb483efe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 147,
                "y": 100
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4797f8c6-62ea-48ca-beed-260b129b9120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 168,
                "y": 100
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "01532452-7668-4bf0-a30a-cf5974b05bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 189,
                "y": 100
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ff3b3dc0-62dc-49f7-90a4-7b93195ed2e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 0,
                "y": 125
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ba49d65a-e7d0-4828-8499-a8a05ca93e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 21,
                "y": 125
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7b9fb12b-1949-4c33-a2a7-8a55aed0c329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 42,
                "y": 125
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "34f04189-0e3e-4037-bdda-1f32653e2766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 63,
                "y": 125
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "85ca5cbc-375b-4661-b6d7-ba53cdd20aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 84,
                "y": 125
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "46c6dba9-b727-4e2e-981e-a6849e639023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 105,
                "y": 125
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d8fbb53e-68f9-4ea2-ab38-0895c90efbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 126,
                "y": 125
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ff98d665-e261-486b-a673-54a67c42a370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 125
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fbca5cd6-cd33-4afb-a36b-06560b19332b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 168,
                "y": 125
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ce8ca1d6-e472-4405-a1be-d7efd26b4f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 189,
                "y": 125
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "eb562fb7-eb8e-4e48-aeb2-0ce25190fa65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 0,
                "y": 150
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ce54ead8-1f00-4fd3-9e06-253e2b009b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 21,
                "y": 150
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a321eb94-2546-4f7e-8ee3-544ce3333007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 150
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3ccde3e0-7d76-4274-9855-063e744a0dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 63,
                "y": 150
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8c940d10-008e-417d-af99-fa6d30bc4165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 84,
                "y": 150
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "87450a20-d00e-42dc-800d-fe3dce641764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 105,
                "y": 150
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1f61a1b3-73f1-434e-ad39-22419edbd86b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 126,
                "y": 150
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ba314b56-8d5c-47ad-a034-25a58778047e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 147,
                "y": 150
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9e4c694f-fdc2-4155-abaf-dd33bd5cbdf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 150
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "142d24a7-a98b-4a9a-82e0-5faf243b9ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 150
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e5b83304-62ed-471e-9a09-2c43562819ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 0,
                "y": 175
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c6b94175-04af-41c3-bba9-56628a041ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 21,
                "y": 175
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f09becc5-4fe0-4c92-bc79-b03a91da38e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 175
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cad024ab-0816-431f-9f36-d39a088ddb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 63,
                "y": 175
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "610c3ac3-c4f6-4696-9f09-3451c8749569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 175
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3468c282-1f84-4fd1-ba6d-7e04956e8cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 175
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d8e81a0d-1add-4c5d-bb04-52997e4ee422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 126,
                "y": 175
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ca891862-e41d-47ab-b566-3ab7476e9b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 147,
                "y": 175
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1e01f528-a8af-4416-a021-431e29c61d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 168,
                "y": 175
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "21a72775-baba-47b0-a720-9f8104426c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 175
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c046a201-b3e7-4c91-b5a9-117132734e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 0,
                "y": 200
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5401293a-6f99-4529-8a78-31437de8c2f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 21,
                "y": 200
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "313bd0f6-e5c3-416a-957e-0a4d6a358b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 200
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ea34b650-25d8-4d4b-ad99-8fbf0e4d7a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 63,
                "y": 200
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b9793fe6-eb02-47ad-911e-8b95f275379c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 84,
                "y": 200
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0239d66a-c582-47ba-a560-ad62b6eedac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 105,
                "y": 200
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9744238e-0bcd-4b64-90dc-57dcdc379f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 126,
                "y": 200
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "225cecae-7a8c-41a0-ab0a-ebe8e052a8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 147,
                "y": 200
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c6bed971-78ad-4c09-bce7-c546ecf377a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 168,
                "y": 200
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c780d6e8-0746-4651-8993-90568e2ad939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 189,
                "y": 200
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e41d50ed-2dc6-4a41-b8b9-5ac95c8c4c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 0,
                "y": 225
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "32b40704-3d9d-409e-96e7-d0e8c8cb6aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 21,
                "y": 225
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a883bc19-f9dc-479d-a960-f9050567a13c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 42,
                "y": 225
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4d212846-c4ca-41a7-ab30-a644776e6949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 225
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "60334ff5-49fd-4682-b308-b32e3541aa06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 84,
                "y": 225
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c03045b0-8cde-4070-8e4f-666f2d30c4a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "52465550-a422-4e99-bbd7-c537061bba6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "5dd5f6d2-2d18-49b1-a085-9118069f015d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "058136b1-e3d9-4da4-8076-0381cafb8a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "379bfac1-b328-4dd2-baf6-2d5325a2efe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "8a9c919d-e194-40ac-ab4b-239d1bbef78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "0394053c-92d3-4391-b1e7-08c967d44257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "3e78a601-76ec-412d-ad2f-d8987c328a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "2069570d-6bec-4ca6-8a26-70105628a94c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a4dc7a0c-08b9-4455-aa1e-f996a3b96d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "46770ca1-6bd3-4b2a-9779-95ce42b84332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "190577b4-9aff-4583-aa2e-41e710b41f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "08cbb694-9d3e-4210-bb0c-24e756aca057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "bcec8d02-bb97-4834-b286-78c858a5222f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "0626f801-2ece-4823-9ffa-f632bc56a3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "0e46d40e-ec3b-4510-a121-d5e3cd10953d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "75e30ec0-1a0a-44b9-b481-47cbd52ed8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "f929a5fb-c05e-4c45-a900-9b2519adfcb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "f6f2ebd0-9341-4aed-ab83-ed35ef1fa2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "39654da2-be67-4bb9-a861-36d1b3baddaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "505dd792-ae70-4748-8d3f-7b9fc1de40b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "e05e722f-2fbf-4f7d-bf79-3394439652b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "23e2c4a3-8a40-4ad6-89d4-9e32b25e09e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "e738d09f-753b-4d63-ba5f-03b27023bb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "239ec69d-385e-4bc9-972c-bb173204db95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "9554c006-60db-4a8b-a7ad-873d853bde97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "144eb020-1a59-4cab-81d4-d7e4031ea31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "00e32d9a-2616-492e-a794-2b2a0f0488dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "48b5f171-1e19-43f4-8632-1f869a479be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "4a63ef81-48fb-4a61-a2b8-0cca072825cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "7b0a3d3c-2e10-454e-8f30-4ccd3631ae05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "7cf09839-a209-4f76-ae12-b559c9ef4b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "ec89c01d-86ef-4623-946f-ca4236d4a551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "e34668b4-5bdf-47bb-be4c-0afddd0d439c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "fcd9e3db-8f2a-4c29-a827-4c7de7ee71c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "11370c68-b860-4730-a3a3-fa8d4468da52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "0227a52b-7df1-4088-b006-3ce2398c3f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "b9372251-74e6-403f-a4a4-4bed1daf64c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "5e68147e-0483-4d87-8b74-c907192c0568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "38779db6-962e-4790-a903-85f2e6fdb5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "69a3682d-7369-4d45-8cfc-b3ad90c4f212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "f73edc3e-c1dc-4d82-a4fb-cb0bb0d30743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d83cef7e-c62b-4187-872f-13bad633e588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "83ecf780-fd15-4e8f-81a9-001fc8036540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "400149f0-7817-48ea-a3cb-a121e7ded531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "656e761b-d86c-463e-9d95-0ae5f380f059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "6374dfd3-55d9-4356-8f68-660974dd1686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "6425cc95-835e-47e1-9fea-0aa15d4923e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "5300dc1e-a117-440a-a6fc-519750d79323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "42f0295f-3f49-40f6-807f-687ce877c812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "a6924022-5743-4589-8d19-c77442543baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "815017f2-7443-4532-bdba-3edb3a2f85d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "e6d4a2ef-e89c-4326-97af-8e5384c4d31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "fb3c3950-4caf-4e88-a809-34410d676553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "aeb549aa-7553-484e-af72-e710021e2a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "b0dd41e7-4262-4541-b59e-f3ba817c238c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "e3f77fb4-9547-4ec2-aabb-0682f1b1f2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "82c8404e-6666-43d8-b409-aaf352efb8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "0f8e3689-45df-465b-af87-04d27c5c6149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "75dc53b4-9175-4cf1-935d-67eabf260db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "fd0a1656-75a1-4773-a13e-be8b78ca45b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "25833254-d86a-4cee-899d-ced47a93e5d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "56600737-a3b3-4af9-881d-390a2c65264b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a76e83d6-2b02-4e7c-ab01-25fb5efd7160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "8fcfc64e-9821-4312-9837-533456dce480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "1d9152d2-dbca-4758-b4e4-a23cff2efb7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "3918e2eb-d4d1-4abc-98cd-c33b5bc2730b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "43e6adb5-8506-430b-8cca-ce8f65398a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "7a6a28ce-20b5-42bf-bf08-56d995470bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "592797a9-64e8-4c85-81d5-0272812b7746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "59edf1f6-3a81-4b59-a086-5aaaddac8535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "3e9bd2d8-8d48-4026-a6a2-1f1586013c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "4d12ba13-26f8-494f-a220-88efbd1a227a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "07f7a88c-dfd6-4073-b7bb-c1c5522558ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ea75c31a-29e9-41ef-b396-50a22d43e172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "81a3b7f3-f2cb-4aa2-8623-f2776655b9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "5284dbe1-1308-4395-a264-fb7ae3c0a3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "d3c7caf3-ee59-49d0-b8e6-4a977fc62688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "0bba127c-200d-446d-bbac-47685278ad00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "b8e596ba-e3a9-4fec-8f0b-beacac1c24ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "1846d1ea-af5b-4efb-88b0-60ccf16ee05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "abed43b6-6021-4841-a1f8-3f581881eed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "2821fae3-ee24-47bb-b474-f5ed1b512fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "bbc28567-d5bb-42d8-b8e4-56ae7bd3c1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "a4505a9c-eec2-450c-9a81-9d082bb02e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "78ec58ea-483f-43e7-a3ff-54a434d8fb21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "a0faaf27-57b6-4247-aaed-11952a58542b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "fa975561-83a5-4602-94eb-93bc4cc5fdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "size": 15,
    "styleName": "Regular",
    "textureGroup": 0
}