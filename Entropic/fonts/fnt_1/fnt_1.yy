{
    "id": "4547eeeb-6822-4d80-bf3d-7380e1726f01",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_1",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "098f3651-9b21-4423-be57-3234ea489198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 0,
                "y": 0
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5b375c23-fe5f-43be-9634-d2e2629218d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 25,
                "y": 0
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "33fca4fc-9c30-492c-9283-227aa9511e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 50,
                "y": 0
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2397ccc0-9bfd-4ecc-9e9d-62316f578713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 75,
                "y": 0
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5fc38c16-0ead-4c4d-ace3-9323c224f222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 100,
                "y": 0
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b106d60f-0ef7-49b3-946e-b4e7af48bc0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 125,
                "y": 0
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a4807ca2-0c34-4b68-9aff-4566d198e225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 150,
                "y": 0
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b721dc5f-90dd-4d11-acfa-7ef8d2ebb0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 175,
                "y": 0
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6c2b762d-d612-444c-961c-193d76d73e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 200,
                "y": 0
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "652c4b26-b33c-489e-a6b3-4d1834c5c12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 225,
                "y": 0
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e8a803d1-bd87-4307-b498-3267c73e9c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 0,
                "y": 30
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fa5b7afe-a462-445f-b268-c036097ebf79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 25,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a900e2f9-1686-465b-bc04-bf67207774f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aaff3e3a-186a-4b36-bbb1-60883d4da26a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5ae97692-2ec5-40b2-86d7-e193b5f6883d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "91dd3491-2eb5-42df-9b8f-1a7052dbe1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 125,
                "y": 30
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "61805619-ced3-4c9e-a237-ab81372acdc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 150,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eacd25b5-0005-436e-bad6-d0455dfd188f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 175,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ea00fab1-99f0-436e-9fda-efff44266b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 200,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "22ffa1a7-b0dc-410d-bbdf-4af515447573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8124fdf5-a025-4c32-95e6-ceb680ed95e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 0,
                "y": 60
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "76fe52a0-d4a5-4008-b6fc-74098719d3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 25,
                "y": 60
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dbf83f0a-4253-4a19-8d44-a5899f93b832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 50,
                "y": 60
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b72de94a-e2da-447e-8896-f7e507e3e5ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 75,
                "y": 60
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "733489c0-0f93-4c49-ab20-a048f918cf08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 100,
                "y": 60
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8bbe9c0d-148c-4202-b6a0-bba7c51602c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 125,
                "y": 60
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6be8aeda-f7cd-4803-9ca8-08f77774dcf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 150,
                "y": 60
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e6ca388e-0837-4255-b9c0-ea710f8e7d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 175,
                "y": 60
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "12d9f10f-b2b5-4251-99c6-dcb7dec84e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 200,
                "y": 60
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "84c18c4f-dd21-43ca-b630-edef33aef3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 225,
                "y": 60
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c20403ac-dc25-470b-92ce-a4b0629d891b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 0,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "441c5e26-a943-42b8-9263-11c01d92aa05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 25,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a7da5a5c-e8d8-49fd-980d-eb53d1065ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 50,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b0d74493-e9a2-4b7f-a8d1-f5ac90f767d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 75,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b93b1083-7ba5-4eb1-ad2e-a8e13ee3685d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 100,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5c00de42-27c7-4a02-9b91-f76e43779235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 125,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2ace9313-394e-499a-994c-3309aae2a65f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 150,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "afb34ebd-5b5b-4807-b54e-a037871d2e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 175,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ff700e48-393e-4578-9112-2c02461a3188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 200,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e4f8d832-953b-44aa-a512-8c7e1d5d48f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 225,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7b5b788c-3906-4729-b7b5-098a31c94804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 0,
                "y": 120
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e9887bb0-8f7b-4a84-a791-8cfa24de900c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 25,
                "y": 120
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a8a07581-9546-4f68-b9c9-004e869e1a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 120
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9c1ed244-435d-4879-a154-965026a54495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 75,
                "y": 120
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8786bbe2-89d5-4f50-966d-3b60fceb490e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 100,
                "y": 120
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "64d2ecd7-cb7d-4c05-89ac-4c7f1db97dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 125,
                "y": 120
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "794b1f0f-3d17-41cf-b508-641e3a3e0f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 150,
                "y": 120
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4c08b284-8395-4d66-ad6a-ff257f0f06db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 175,
                "y": 120
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e0f80775-92c5-4943-b78a-2260f44f2fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 200,
                "y": 120
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "88bc291d-e94f-49db-90f6-fc32554cddc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 225,
                "y": 120
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fe0956e9-04d1-437b-9060-4a1fd1e2fe1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 0,
                "y": 150
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "43693038-b820-471e-842e-24dd0cac2d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 25,
                "y": 150
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4bfd90f6-4d20-4455-8e8f-0fd74a8e35df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 150
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "508e5e06-4fdd-439a-9340-78cf15c7f37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 75,
                "y": 150
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "90f2da79-38da-4377-8889-33032d5de11d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 100,
                "y": 150
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "39a2d1c7-4c96-49a8-8b2b-ae1572ee282a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 125,
                "y": 150
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a450c875-8d7b-41ee-8b9f-a2dfe96a2ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 150,
                "y": 150
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6f3cdd49-d579-4927-b497-5731eff00e8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 175,
                "y": 150
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b34c2b70-77fe-4bbe-a291-a48d5a280f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 200,
                "y": 150
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f097780b-1404-4b96-8b75-447679909045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 225,
                "y": 150
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "42c2efb7-eac1-49d7-9bc2-e8e167a08135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 0,
                "y": 180
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "296d562f-3f99-43ab-8500-a337555e72d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 25,
                "y": 180
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e2fe2186-2561-4bff-a4d6-9e53eebc9b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 180
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7fbdaed9-e38e-465d-a88c-4343d23b0ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 75,
                "y": 180
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "860d7747-d0fc-4f05-9d79-7d065826367e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 100,
                "y": 180
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2b7cab3b-f154-4f31-9863-cab1858babd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 125,
                "y": 180
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c705a99c-3806-4236-9ab5-cde3e66748e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 150,
                "y": 180
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6b4d4bb9-ab4f-4474-be1e-abd0dd21dc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 175,
                "y": 180
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a3cd6584-838b-4dbe-9489-0e089729713b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 200,
                "y": 180
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0824f07b-5cd8-4ab2-8687-3fea929f0c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 180
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "05d33fd3-2573-43ac-91e2-6b10d45dd461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 0,
                "y": 210
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5b7b23f1-2818-41c2-9bb3-fb57da6623a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 25,
                "y": 210
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "31c3738f-233b-4964-9c1d-5b1ddf53299c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 50,
                "y": 210
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3319e9b2-3a36-43f1-8d33-78b1e0a89189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 75,
                "y": 210
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1cd52260-301c-457e-99e8-d99819eb1d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -2,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 210
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8b405e1a-ed8a-46d3-a46e-ab83ef302f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 125,
                "y": 210
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "36f8f533-8ae6-4119-a2d6-a54edcb34ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 210
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "de63ccc2-1db7-4c17-a8d7-1e48ead89034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 175,
                "y": 210
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ea0bb145-b1af-4c92-a0ae-9202ddbc5445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 200,
                "y": 210
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "65f46b66-f6ca-49b2-be6d-2b0529bd9651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 210
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f1db9520-513f-423d-9022-13cb5bd1dde4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 0,
                "y": 240
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "36ec090c-c369-4dfd-aef7-9096a91d3824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 25,
                "y": 240
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7506dcd9-eecc-4c77-a67d-577b0c5d3f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 240
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "542d5d84-cf49-4696-b1ba-6a8596952a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 75,
                "y": 240
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "57855d91-4138-4647-819d-238674f5db12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 240
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b1ae7b9c-7040-4169-bb5f-e6ee609ff50a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 125,
                "y": 240
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0046ee45-0d20-4bdf-9536-b9719fa4b49b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 150,
                "y": 240
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8054a9d7-a557-4fd1-926b-383c017c890a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 175,
                "y": 240
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5407922c-dcdd-461a-ae64-be6f5e827538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 200,
                "y": 240
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0267393a-6307-4325-95e2-f54c1dc17170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 225,
                "y": 240
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1fa6f0a6-1291-4d18-85bf-9bbacdca9cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 0,
                "y": 270
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4aa5d275-e38c-4b07-80d2-f5209c971d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 25,
                "y": 270
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f2967ba7-179e-425a-b514-36cceafbf46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 50,
                "y": 270
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "49df8214-2596-49bf-a694-24d99e59c115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 75,
                "y": 270
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5558d9ef-ae10-4abf-8e89-eaabdcb15eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 270
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1c91319a-ba71-43f6-940e-43d34c9374b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "3e5847ae-8eb4-4339-9774-715871ca1f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "6e3ac15c-a587-4cb1-8711-5a5ba7bd8be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "7a067ab4-799a-4762-8597-6393dd3f557c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "1bd0322f-ec5f-470f-ab86-cc841a69ac0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "516a82ee-b234-4ced-b198-fc01c9c951a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "14f92781-ecfe-40f0-bcbd-dc58c73aaec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "ee7ffe23-11aa-42ea-be6d-91c43332c607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "3bae1e03-1e74-46d8-847d-6a1c8899a204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "1c7bbbd1-df08-495a-9719-89cde43d6b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e2c42e8e-a39f-4206-aa8e-56998fd33ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "9bacd972-25c8-4b5a-94f9-85a62fb9a996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "b8f2fd69-316b-4322-93b7-34b04774bf85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "04e6270b-000a-4f32-aa48-104e37ae4482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "93633dca-209c-4f28-8f6a-d709da696dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "79c0feb4-9126-451e-ba7b-283795559180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "86530bb6-9313-4110-b395-8cb36e5464b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "26eb4531-0dd5-4ad0-b523-1013b74ef00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "ad93a4db-bebe-4f31-a05f-94b1d4dc067e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "0010a737-7076-45fc-84cf-f5382d663063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "c273f099-dcda-4d3e-97d1-25a039080959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "215a7a10-8a90-432a-ab75-0b72aede834e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "be507da7-1468-470a-b825-9a84555f19fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "d09d03fe-2166-4ef1-91d9-cba7d738d288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "a33bb18b-a51b-41e7-8307-595600c626f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "8d8dce62-19f9-4235-bcb8-3d48b7c53b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "f8f23f92-3fdc-45d0-8a6b-977b866f940b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "e614912e-d8c9-4c43-8526-5f002785e2cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "787ec111-fddb-4c14-9ddd-343f7c14df28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "38e40617-d85d-45ef-980c-2807ff753e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "9291be30-121b-4651-9366-934fb54d443c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "3c5fc742-5c60-4f1d-8159-fce95bf47ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "8bc0b38b-3946-4c04-83c0-e12def4708f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "22b8e6a2-af3c-431a-bbf1-feca03704bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "5eba0d9a-6e9d-4581-b103-ff5b83b4d2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "4ae934c9-cd1a-4c20-bc97-ebbf8d033ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "82c1a75f-ac13-49c7-8d19-2fc3c6c460b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "19663c71-ce67-4f64-ae25-82bf17d5898a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "9be6b24b-abee-4b2f-b5a1-571e77d636ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "b08b127b-634b-4ee0-8747-1a6f53b81fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "6a7ceac3-6c16-4e7d-b17a-2bfc057f20e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "90473d17-7ca1-4407-a2f8-79c857110cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8591a7c9-5b6d-47e2-9aaf-afd0e33ec504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "f985f226-5139-4b04-9af1-4cdd47333185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "0f36e1de-5507-4dc0-9839-ced824dc0ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "e0e8fa30-2fe8-4d16-bd94-18fd773a5c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "b4b72735-9f45-4f23-aed1-8dde72b6b096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "372c8626-5621-4f0f-bc78-1fb81d212ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "ffd2c4cf-01ca-4db0-9a8c-37194739cb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "b889a9ab-1ec4-4b9d-8cd3-ed1d5d43cf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "5cc7e6c5-b493-4e4f-b380-4d265f4eb892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "be927b1a-02c1-4543-8ee1-5f7e37088425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "a5448389-8930-4ec7-8dac-cea889131146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "ebf0ba1c-a1cf-43b4-b94b-ff56528ec4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "80009efe-1fb5-49c7-9620-6197af8cf9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "ed833bbc-f6ef-4baa-b1df-f530c1af3a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "dc43e8cc-6c47-4d49-87ad-eb2b2be5465e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "21ff7e16-b8ac-4474-bf0e-80c3ad3887a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "802d1139-c36b-4710-b413-72ea550a6980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "fb3b02a9-007f-4bff-890b-23f9265357bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "119fd1bc-49ee-4cbb-90a8-1016480cc3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "3b9a63cf-3d3c-4773-91f4-e3bb740eb789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "f7dac9f6-5c20-4829-a6f7-f6758c7a1475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "ee0702df-9b7d-4e5f-a217-ba3fb5afa148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "da96e524-6dc0-4b45-9749-9e9fad5192a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "5f1cddd3-7c9b-43dc-98b6-946fc80d0bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "cfb3624b-e01c-4bcd-a9f4-d388212a1759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "807a89dd-d2b7-4abf-8eb4-c06f011b7b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "51e5315e-730f-48ea-a2bc-7335660e8b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "0e6b2e5a-cb1f-4ea5-a8f8-6cb63ee1ef11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "ab581937-aea4-4bf9-a3fc-f23de2021bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "6bc5b2f9-4710-4d3e-9875-9062aa8e9f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "a9cf1a27-394b-4ca1-bc59-4a43e211b756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "6746502f-6bb5-4f73-94bc-b398770c6a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "c0f939c0-e3fd-4e12-aa24-972b7a4ad855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "3fbd0786-4590-4b34-a525-830978978e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "3ece41ea-0d0b-439f-8ebf-d2eeacbe510f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "c8b097a7-c613-4542-971d-2bd90dee6002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "9a2461ea-761c-4697-add1-0f8872ce18e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "a89438af-5dea-4984-87d0-cf5693d610cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "7e56f9fd-7175-496e-8862-045827b6c44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "5a329f60-fdaa-4aec-83c5-5b809b011f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "7a164d29-5468-4f51-a368-908300a7a46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "dda95a74-956d-421e-852a-02bf307c0eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "c083f34e-9cd7-4214-bc7b-b5579ce899c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "a56b16f9-d941-43a9-b0d4-2a8016c2bb58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "80a54e2d-e101-4e24-a86f-5f40afe35962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "d1273228-f6c9-4095-a49f-cef6700bf79e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "size": 18,
    "styleName": "Regular",
    "textureGroup": 0
}