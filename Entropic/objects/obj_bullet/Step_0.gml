if (global.isRecording == true)//if we're recording
{
    //save our state into this object's local position array
    pArray[global.recordedStep, 0] = x;       
    pArray[global.recordedStep, 1] = y;
    pArray[global.recordedStep, 2] = image_angle;
	pArray[global.recordedStep, 4] = new;
}

new = false; // set new to false

if (global.control == false)//if going back in time
{
    //if we're going back to when bullet was new, destroy it
	if (global.recordedStep > 1)
	{
		new = pArray[global.recordedStep-1, 4];
	}
		
	if ((new == true) || (global.recordedStep < 1))
	{
		instance_destroy();
	}
    else
    {
        //set our variables as those in the local position array
        //REMEMBER the -1 after global.recordedStep!!!
        x = pArray[global.recordedStep-1, 0];
        y = pArray[global.recordedStep-1, 1];
        image_angle = pArray[global.recordedStep-1, 2];
		new = pArray[global.recordedStep-1, 4];
    }
}

// doublecheck we're destroying correctly
if (new == true)
{
	instance_destroy();
}