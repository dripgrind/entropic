{
    "id": "d0e8007c-c678-4770-9905-4a737018e8ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "4e6aa241-5c22-4a6c-94ee-d09231676a73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d0e8007c-c678-4770-9905-4a737018e8ec"
        },
        {
            "id": "c06ede7a-e632-4105-943a-962c85b93ed6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0e8007c-c678-4770-9905-4a737018e8ec"
        },
        {
            "id": "4c8eed65-fc4c-407d-83dd-58674b8b8219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "72ac80d2-e603-424a-9a67-646c200396b7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d0e8007c-c678-4770-9905-4a737018e8ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f3efff10-13b9-4016-a88c-291216c032b8",
    "visible": true
}