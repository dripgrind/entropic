if (global.isRecording == true)//if we're recording
{
    //save our state into this object's local position array
    pArray[global.recordedStep, 0] = x;       
    pArray[global.recordedStep, 1] = y;
    pArray[global.recordedStep, 2] = image_angle;
	pArray[global.recordedStep, 3] = cooldown;
}

if (global.control == true)//if we have control, i.e. We're moving ourselves
{
	visible = true;
	//decrement cooldown timer
	cooldown = cooldown -1;
	
	if (hit == false) //if not shot
	{
	    //do some basic movement
	
	    image_angle = point_direction(x, y, mouse_x, mouse_y);
    
	    if (keyboard_check(ord("W")))
	    {
	        y -= playerspd;
	    }
    
	    if (keyboard_check(ord("S")))
	    {
	        y += playerspd;
	    }
    
	    if (keyboard_check(ord("A")))
	    {
	        x -= playerspd;
	    }
    
	    if (keyboard_check(ord("D")))
	    {
	        x += playerspd;
	    }
	
		if (keyboard_check(ord("R")))
		{
			room_restart();
		}
	
		if ( (keyboard_check(ord(" "))) && (cooldown <1) )
		{

	
			//fire a bullet
			var xpos = x + lengthdir_x (bullet_offset, image_angle);
			var ypos = y + lengthdir_y (bullet_offset, image_angle);
			var bullet;
			bullet = instance_create_layer(xpos, ypos, "Instances", obj_bullet);
			cooldown = 10;
			with (bullet)
			   {

			   direction = other.image_angle;
			   speed = bulletspd;
		
			   }
		
		}
	
	}
	else // if hit is true, create player ghost and rewind time
	{
		playerghost = instance_create_layer(x, y, "Instances", obj_PlayerGhost);
		//show_message(playerghost);//debug to check ghost created
		playerghost.pArray = pArray;
		playerghost.maxStep = global.recordedStep;
		hasghost = true;
		//show_message(pArray);
		//playerghost.startStep = global.recordedStep - global.max_rewind_steps;
		//if (playerghost.startStep < 1)
		//{
		//	playerghost.startStep = 1;
		//}
		obj_SpaceTimeController.rewind = true;
		hit = false;
		visible = false;
	}
}
else//otherwise, if we have no control, i.e. We're travelling back in time
{
    //as long as there's a previously saved step to go to (NOTE: Game Maker seems to crash sometimes if this is: if (global.recordedStep > 0)
    //works fine as below
    if (global.recordedStep > 1)
    {
        //set our variables as those in the local position array
        //REMEMBER the -1 after global.recordedStep!!!
        x = pArray[global.recordedStep-1, 0];
        y = pArray[global.recordedStep-1, 1];
        image_angle = pArray[global.recordedStep-1, 2];
		cooldown = pArray[global.recordedStep-1, 3];
    }
}


