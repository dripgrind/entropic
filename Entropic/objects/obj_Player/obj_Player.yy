{
    "id": "d13769bf-0737-4a81-9d2e-4950105ac2c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "d8411b0c-a8e1-49d7-bba6-303968a3b2af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d13769bf-0737-4a81-9d2e-4950105ac2c3"
        },
        {
            "id": "8c83bda7-6040-412d-beca-fd3fa4e9ea8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d13769bf-0737-4a81-9d2e-4950105ac2c3"
        },
        {
            "id": "fc24e594-fb9e-4fbb-9221-08f516909348",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d0e8007c-c678-4770-9905-4a737018e8ec",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d13769bf-0737-4a81-9d2e-4950105ac2c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "72ac80d2-e603-424a-9a67-646c200396b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3b7b6436-5edc-44cb-9bff-1a82cf6c255e",
    "visible": true
}