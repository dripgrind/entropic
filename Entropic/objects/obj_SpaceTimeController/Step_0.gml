/***************************************************
  Space Time Controller - Created by SlasherX
  as seen on YouTube.
  This controller handles whether step recording 
  should be done, at what speed we go back in time
  and whether the player is going back in time.
 ***************************************************/

if (global.isRecording == true)//if we're recording (which is default)
{
    global.recordedStep++;//increase the global record step
}

if (global.control == true)//if we have control
{
    global.stepMultiplier = 1;//then we will always be at a step multiplier of 1
}
else//otherwise if we have no control, i.e. we're stepping back in time
{
    if (global.recordedStep > 1) && (rewind_steps > 0)//if recording available and we haven't gone back max
    {
        if ((global.recordedStep - global.stepMultiplier) > 1)//check that we can decrease by the stipulated multiple
        {
            global.recordedStep -= global.stepMultiplier;//decrease by multiplier
			rewind_steps -= global.stepMultiplier;// count how many steps we've gone back
        }
        else//otherwise just set to one
        {
            global.recordedStep = 1;//there are now no more recorded steps left
        }
    }
    else
    {
        global.control = true;//end of rewind means we now have control again
		global.isRecording = true;//we're recording steps
		rewind_steps = global.max_rewind_steps;
		rewind = false; //stop trying to rewind
		obj_PlayerGhost.rewindcount++;
		//global.recordedStep = 1; //reset recorded step count
    }
}

if (rewind == true) && (global.control = true) && (global.recordedStep > 1)//return to rewind
{
    if (global.recordedStep > 1)//as long as there are recorded steps to go to
    {
        global.isRecording = false;//we're not recording
        global.control = false;//we have no control
    }
}


