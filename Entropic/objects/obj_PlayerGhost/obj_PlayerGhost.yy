{
    "id": "871ea25f-d4e8-41df-83d2-0cd914e989b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PlayerGhost",
    "eventList": [
        {
            "id": "765346d3-f838-4fff-979e-a49ba7d14783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "871ea25f-d4e8-41df-83d2-0cd914e989b7"
        },
        {
            "id": "510cfdd0-5c9d-4a86-9a5f-fae82acbafc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "871ea25f-d4e8-41df-83d2-0cd914e989b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "72ac80d2-e603-424a-9a67-646c200396b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3c2677a0-da2c-4981-af9c-ab622f862eb1",
    "visible": true
}