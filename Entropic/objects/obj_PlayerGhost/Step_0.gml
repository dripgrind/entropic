/// @description 

if (global.isRecording == true)//if we're recording
{

	//if (rewindcount > 2) //if it's the first rewind after the original one
	//	{
	//		instance_destroy();
	//	}
}

if (global.control == true)//if time is going forward
{
	
	if (global.recordedStep >= maxStep)
		{
			instance_destroy();
			obj_Player.hasghost = false;
		}
	else
		{
		//play back from pArray copied from player on creation
		x = pArray[global.recordedStep, 0];       
		y = pArray[global.recordedStep, 1];
		image_angle = pArray[global.recordedStep, 2];
		cooldown = pArray[global.recordedStep, 3];
		}
}


else// if we're rewinding
{
   
  if (global.recordedStep > 1)
    {
        //set our variables as those in the local position array
        //REMEMBER the -1 after global.recordedStep!!!
        x = pArray[global.recordedStep-1, 0];
        y = pArray[global.recordedStep-1, 1];
        image_angle = pArray[global.recordedStep-1, 2];
		cooldown = pArray[global.recordedStep-1, 3];
    }

}


