if (global.isRecording == true)//if we're recording
{
    //save our state into this object's local position array
    pArray[global.recordedStep, 0] = x;       
    pArray[global.recordedStep, 1] = y;
    pArray[global.recordedStep, 2] = image_angle;
	pArray[global.recordedStep, 3] = cooldown;
	pArray[global.recordedStep, 5] = hit;
	pArray[global.recordedStep, 6] = dead;
	pArray[global.recordedStep, 7] = sprite_index;
}

if (global.control == true)//if we have control, i.e. We're moving ourselves
{
	cooldown = cooldown -1;
	//if not hit or dead, do some basic movement
    if ((hit == false) && (dead == false))
	{
		var target;
		if instance_exists(obj_PlayerGhost)
		{
			target = instance_nearest(x, y, obj_PlayerGhost);
		}
		else
		{
		 target = instance_nearest(x, y, obj_Player);
		}

		image_angle = point_direction(x, y, target.x, target.y);
	
		if (distance_to_object(target) > firing_distance)
		{
			move_towards_point( target.x, target.y, enemyspd );
		}
		else
			{
				speed = 0;
				//fire a bullet
				if (cooldown < 1)
				{
					var xpos = x + lengthdir_x (bullet_offset, image_angle);
					var ypos = y + lengthdir_y (bullet_offset, image_angle);
					var bullet;
					bullet = instance_create_layer(xpos, ypos, "Instances", obj_bullet);
					cooldown = 10;
					with (bullet)
					{
						 direction = point_direction(x, y, target.x, target.y);
						 speed = bulletspd;
					}
					
				}
			}
	}
	
	else if (hit = true)
	{
		sprite_index = spr_EnemyDead;
		dead = true;
		hit = false;
		speed = 0;
	}
	
}
else//otherwise, if we have no control, i.e. We're travelling back in time
{
    //as long as there's a previously saved step to go to (NOTE: Game Maker seems to crash sometimes if this is: if (global.recordedStep > 0)
    //works fine as below
    if (global.recordedStep > 1)
    {
        //set our variables as those in the local position array
        //REMEMBER the -1 after global.recordedStep!!!
        x = pArray[global.recordedStep-1, 0];
        y = pArray[global.recordedStep-1, 1];
        image_angle = pArray[global.recordedStep-1, 2];
		hit = pArray[global.recordedStep-1, 5];
		dead = pArray[global.recordedStep-1, 6];
		sprite_index = pArray[global.recordedStep-1, 7];
    }
}


