{
    "id": "326d7c73-d16f-452b-b192-2620aed303b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Enemy",
    "eventList": [
        {
            "id": "05968766-31f1-4fbb-9068-01309b18f817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "326d7c73-d16f-452b-b192-2620aed303b3"
        },
        {
            "id": "0eba9f21-e5d0-492b-b54d-fb5b97e6ff14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "326d7c73-d16f-452b-b192-2620aed303b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "72ac80d2-e603-424a-9a67-646c200396b7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e7790200-3d7f-4488-a21d-2aa93b787ac4",
    "visible": true
}