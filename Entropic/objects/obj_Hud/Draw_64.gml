
draw_set_font(fnt_1);
draw_set_colour(c_white);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_text(room_width/2, 600, string_hash_to_newline("recordedStep: " + string(global.recordedStep)));//draw how hand steps we've recorded or have left
draw_text(room_width/3, 600, string_hash_to_newline("playbackStep: " + string(global.playbackStep)));

//If we're recording, then draw the rec box
if (global.isRecording == true)
{
    draw_set_colour(c_yellow);
    draw_text(95, 20, string_hash_to_newline("FPS: " + string(fps)));
    draw_set_colour(c_white);
    
    if (showRec == true)
    {
        draw_sprite(spr_rec, 0, 140, 88);
    }
}
else
{
    draw_text(room_width/5*4, 40, string_hash_to_newline("Not recording!"));
    draw_sprite(spr_rewind, 0, 140, 88);
}

if (global.control == false)
{
    draw_text(room_width/2, 18, string_hash_to_newline("Playback speed: " + string(global.stepMultiplier) + "x"));//show the user how fast were stepping back at
}



