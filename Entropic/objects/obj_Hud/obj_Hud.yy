{
    "id": "6dbb6463-8486-4a51-8adb-1d34b170c8a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Hud",
    "eventList": [
        {
            "id": "905d42d2-9698-40df-b3fe-6d6efc9ddb61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6dbb6463-8486-4a51-8adb-1d34b170c8a5"
        },
        {
            "id": "ecd3d05b-ee6e-4730-9731-89f9d45d2c2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6dbb6463-8486-4a51-8adb-1d34b170c8a5"
        },
        {
            "id": "2a2a0b53-5dc2-432f-bf9d-9c39128334e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6dbb6463-8486-4a51-8adb-1d34b170c8a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}